//Leer un número entero y determinar a cuánto es igual la suma de sus digitos
function sumadigitos(numero) {
formato_string=String(numero),
transforma_array=formato_string.split(''),
suma_digitos=transforma_array.reduce(function(valorAnterior, valorActual, indice, vector){
return 1*valorAnterior + (isNaN(1*valorActual)? 0: 1*valorActual);
},0);
return suma_digitos;
}

console.log(sumadigitos(56));

console.log(sumadigitos('12a3'));