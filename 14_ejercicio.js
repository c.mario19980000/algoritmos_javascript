//Leer un número entero y mostrar en pantalla su tabla de multiplicar

for (let tabla = 1; tabla < 10; tabla++) {
console.log(`\nTabla del ${tabla}`);
for (let numero = 0; numero < 11; numero++) {
console.log(`${tabla} X ${numero} = ${tabla * numero}`);
}
}