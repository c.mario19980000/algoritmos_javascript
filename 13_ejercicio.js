//Generar todas las tablas de multiplicar del 1 al 10

function creandoTablasDeMultiplicarConCicloWhile() {
let tabla = 1;
let numero = 0;

while (tabla < 11) {
console.log(`\nTabla del ${tabla}`);
numero = 0;
while (numero < 11) {
console.log(`${tabla} X ${numero} = ${tabla * numero}`);
numero++;
}
tabla++;}
}

creandoTablasDeMultiplicarConCicloWhile()